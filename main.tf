# Docker Resources
resource "docker_container" "sonarqube" {
  name  = "sonarqube"
  image = "sonarqube:7.9-community"
  ports {
    internal = "9000"
    external = "9000"
  }
}

resource "docker_container" "jenkins" {
  name  = "jenkins"
#   image = "jenkins/jenkins:lts-centos7"
  image = var.jenkins_image
  volumes {
    host_path      = "/home/ubuntu/jenkins"
    container_path = "/var/jenkins_home"
  }
  ports {
    internal = "8080"
    external = "9191"
  }
}
